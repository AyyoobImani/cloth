package ir.hisis.cloth;


import com.etsy.android.grid.StaggeredGridView;

import ir.hisis.cloth.MainActivity.mainViewFragment.ImageAdapter;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class CategoryFragment extends Fragment {
	
	private  String category;
	private static final String CATEGORY_NAME = "category_name";
	private static final String TAG = CategoryFragment.class.toString();
	private boolean isLiked;
	private static final String IS_LIKED = "isliked";
	private MenuItem likeMenuItem;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		category = getArguments().getString(CATEGORY_NAME);
		isLiked = getArguments().getBoolean(IS_LIKED);
		setHasOptionsMenu(true);
		Log.w(TAG, "oncreate");
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		Log.w(TAG, "viewcreated");
		return rootView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.w(TAG, "created");
	/*	((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
		((ActionBarActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.actionbar_view);*/
		StaggeredGridView gridView = (StaggeredGridView) getView().findViewById(R.id.grid_view);
		Context context = getActivity().getApplicationContext();
		gridView.setAdapter(new ImageAdapter(context));
	}
	
	
	public static CategoryFragment newInstance(String categoryName, boolean isLiked){
		CategoryFragment cf = new CategoryFragment();
		Bundle arguments = new Bundle();
		arguments.putString(CATEGORY_NAME, categoryName);
		cf.setArguments(arguments);
		
		return cf;
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.category, menu);
		likeMenuItem = menu.findItem(R.id.like_action);
		updateLikeUi();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_explore_category:
			Intent intent = new Intent(getActivity(), ExploreActivity.class);
			
			startActivity(intent);
			break;
		case R.id.like_action:
			if(isLiked){
				unlikeCategory();
			}
			else{
				likeCategory();
			}
			break;
		default:
			break;
		}
		
		return true;
	
	}
	
	private void likeCategory(){
		isLiked = true;
		updateLikeUi();
	}
	
	private void unlikeCategory(){
		isLiked = false;
		updateLikeUi();
	}
	private void updateLikeUi(){
		if(isLiked){
			likeMenuItem.setIcon(android.R.drawable.star_big_on);
		}
		else{
			likeMenuItem.setIcon(android.R.drawable.star_big_off);
		}
	}
	
	
	public class ImageAdapter extends BaseAdapter {
	    private Context mContext;

	    public ImageAdapter(Context c) {
	        mContext = c;
	    }

	    public int getCount() {
	        return mThumbIds.length;
	    }

	    public Object getItem(int position) {
	        return null;
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            imageView.setAdjustViewBounds(true);
	            
	            imageView.setLayoutParams(new GridView.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.MATCH_PARENT));
	            //imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            //imageView.setPadding(18, 18, 18, 18);
	            
	        } else {
	            imageView = (ImageView) convertView;
	        }

	        imageView.setImageResource(mThumbIds[position]);
	        return imageView;
	    }

	    // references to our images
	    private Integer[] mThumbIds = {
	    		R.drawable.logo_default,
	            R.drawable.sample_2, R.drawable.sample_3,
	            R.drawable.sample_4, R.drawable.sample_5,
	            R.drawable.sample_6, R.drawable.sample_7,
	            R.drawable.sample_0, R.drawable.sample_1,
	            R.drawable.sample_2, R.drawable.sample_3,
	            R.drawable.sample_4, R.drawable.sample_5,
	            R.drawable.sample_6, R.drawable.sample_7,
	            R.drawable.sample_0, R.drawable.sample_1,
	            R.drawable.sample_2, R.drawable.sample_3,
	            R.drawable.sample_4, R.drawable.sample_5,
	            R.drawable.sample_6, R.drawable.sample_7
	            
	    };
	}
}
