package ir.hisis.cloth;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

public class Login extends Activity {
	
	Button loginButton;
	EditText username;
	EditText password;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.login);
		
		loginButton = (Button) findViewById(R.id.btnlogin);
		username = (EditText) findViewById(R.id.edtUsernameLogin);
		password = (EditText) findViewById(R.id.edtPasswordLogin);
		
		
		
		Animation animButton = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.abc_slide_in_bottom);
		animButton.setDuration(2000);
		animButton.setStartOffset(1000);
		loginButton.startAnimation(animButton);
		
		Animation animUsername = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.user_name);
		animUsername.setStartOffset(1000);
		username.startAnimation(animUsername);
		
		Animation animPassword = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.password);
		animPassword.setStartOffset(1000);
		password.startAnimation(animPassword);
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	
		
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//ObjectAnimator.ofFloat(loginButton, "translationY",-loginButton.getHeight()).start();

	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.i("location", loginButton.getHeight()+": "+loginButton.getY());
		super.onPause();
		
	}
}
