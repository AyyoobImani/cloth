package ir.hisis.cloth;

import ir.hisis.cloth.Utils.UtilUser.PasswordMatchException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends ActionBarActivity {
	
	private static String TAG = RegisterActivity.class.toString();
	
	Button registerBtn;
	EditText userNameETxt;
	EditText passwordETxt;
	EditText repeatPasswordETxt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_register);
		
		userNameETxt = (EditText) findViewById(R.id.edtTxtUserNameRegister);
		passwordETxt = (EditText) findViewById(R.id.edtTxtPasswordRegister);
		repeatPasswordETxt = (EditText) findViewById(R.id.edtTxtRepeatPassword);
		registerBtn = (Button) findViewById(R.id.btnRegister);
		registerBtn.setOnClickListener(new RegBtnClickListener());
	}
	
	
	public class RegBtnClickListener implements View.OnClickListener{
		@Override
		public void onClick(View arg0) {
			try {
				ir.hisis.cloth.Utils.UtilUser.registerUser(userNameETxt.getText().toString(),
						passwordETxt.getText().toString(), repeatPasswordETxt.getText().toString(), getApplication());
			} catch (PasswordMatchException e) {
				Toast.makeText(RegisterActivity.this, R.string.passwords_doesnt_match, Toast.LENGTH_SHORT).show();
				return;
			
			}
			startActivity(new Intent(RegisterActivity.this ,MainActivity.class));
			RegisterActivity.this.finish();
		}
	}
}
